package com.company;
import java.util.*;

public class Main{
    public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		boolean x = true;
		String cek = " ";
		int pilih = 0;
		int menu_pilih = 0;
		int ayam = 0, ikan = 0, cumi = 0;
		int jumlah_ayam = 0, jumlah_ikan = 0, jumlah_cumi = 0;
		int total_tagihan = 0;
		int bayar = 0;
		int kembalian = 0;
		while (x == true) {

			do {
				cek = "Y";
				System.out.println("=== RESTORAN DEWA ===");
				System.out.println("1. Pemesanan");
				System.out.println("2. Pembayaran");
				System.out.println("3. Keluar");
				System.out.println("=====================");
				System.out.print("Pilihan : ");
				try {
					pilih = in.nextInt();
					if (pilih != 1 && pilih != 2 && pilih != 3) {
						cek = "N";
					}
				} catch (Exception a) {
					cek = "N";
					in.nextLine();
				}
				if (cek == "N") {
					System.out.println("Input salah ");
				}
			} while (cek == "N");

			switch (pilih) {
				case 1:
					do {
						cek = "Y";
						System.out.println(" ");
						System.out.println("=== MENU MAKANAN RESTORAN DEWA ===");
						System.out.println("1. Ayam bakar \t (Rp 10.000)");
						System.out.println("2. Ikan bakar \t (Rp 15.000)");
						System.out.println("3. Cumi bakar \t (Rp 20.000)");
						System.out.println("4. Kembali");
						System.out.println("==================================");
						System.out.print("Pilih : ");
						try {
							menu_pilih = in.nextInt();
							if (menu_pilih != 1 && menu_pilih != 2 && menu_pilih != 3) {
								cek = "N";
							}
						} catch (Exception e) {
							cek = "N";
							in.nextLine();
						}
					} while (cek == "N");

					switch (menu_pilih) {
						case 1:
							ayam = 1;
							do {
								cek = "Y";
								System.out.println(" ");
								System.out.print("Jumlah pemesanan (Maks 10 porsi) : ");
								try {
									jumlah_ayam = in.nextInt();
									if (jumlah_ayam < 1 || jumlah_ayam > 10) {
										cek = "N";
									}
								} catch (Exception e) {
									cek = "N";
									in.nextLine();
								}
								if (cek == "N") {
									System.out.println(" ");
									System.out.println("Input salah");
								}
							} while (cek == "N");
							System.out.println(" ");
							System.out.println("Anda telah memesan " + jumlah_ayam + " ayam bakar");
							System.out.println("Silahkan pergi ke menu pembayaran untuk membayar tagihan");
							System.out.println(" ");
							break;
						case 2:
							ikan = 1;
							do {
								cek = "Y";
								System.out.println(" ");
								System.out.print("Jumlah pemesanan (Maks 10 porsi) : ");
								try {
									jumlah_ikan = in.nextInt();
									if (jumlah_ikan < 1 || jumlah_ikan > 10) {
										cek = "N";
									}
								} catch (Exception e) {
									cek = "N";
									in.nextLine();
								}
								if (cek == "N") {
									System.out.println(" ");
									System.out.println("Input salah");
								}
							} while (cek == "N");
							System.out.println(" ");
							System.out.println("Anda telah memesan " + jumlah_ikan + " ikan bakar");
							System.out.println("Silahkan pergi ke menu pembayaran untuk membayar tagihan");
							System.out.println(" ");
							break;
						case 3:
							cumi = 1;
							do {
								cek = "Y";
								System.out.println(" ");
								System.out.print("Jumlah pemesanan (Maks 10 porsi) : ");
								try {
									jumlah_cumi = in.nextInt();
									if (jumlah_cumi < 1 || jumlah_cumi > 10) {
										cek = "N";
									}
								} catch (Exception e) {
									cek = "N";
									in.nextLine();
								}
								if (cek == "N") {
									System.out.println(" ");
									System.out.println("Input salah");
								}
							} while (cek == "N");
							System.out.println(" ");
							System.out.println("Anda telah memesan " + jumlah_cumi + " cumi bakar");
							System.out.println("Silahkan pergi ke menu pembayaran untuk membayar tagihan");
							System.out.println(" ");
							break;
						case 4:
							break;
					}
					break;
				case 2:
					System.out.println(" ");
					System.out.println("=== PEMBAYARAN ===");
					if (ayam == 0 && ikan == 0 && cumi == 0) {
						System.out.println(" ");
						System.out.println("Anda belum memesan makanan apapun");
						System.out.println("Silahkan kembali ke menu pemesanan makanan untuk memesan");
						System.out.println(" ");
						break;
					}

					int harga_ayam = 0, harga_ikan = 0, harga_cumi = 0;
					if (ayam == 1) {
						harga_ayam = 10000 * jumlah_ayam;
					}
					if (ikan == 1) {
						harga_ikan = 15000 * jumlah_ikan;
					}
					if (cumi == 1) {
						harga_cumi = 20000 * jumlah_cumi;
					}
					total_tagihan = harga_ayam + harga_ikan + harga_cumi;
					System.out.println(" ");
					System.out.println("Pesanan anda adalah : ");
					System.out.println(jumlah_ayam + " ayam bakar \t : Rp " + harga_ayam);
					System.out.println(jumlah_ikan + " ikan bakar \t : Rp " + harga_ikan);
					System.out.println(jumlah_cumi + " cumi bakar \t : Rp " + harga_cumi);
					System.out.println("=== Total tagihan : Rp " + total_tagihan + " ===");

					do {
						cek = "Y";
						System.out.println(" ");
						System.out.print("\n Uang anda : ");
						try {
							bayar = in.nextInt();
							if (bayar < total_tagihan) {
								cek = "N";
								System.out.println("Uang anda tidak cukup");
							}
						} catch (Exception e) {
							cek = "N";
							System.out.println("Input salah");
							in.nextLine();
						}
					} while (cek == "N");

					if (bayar == total_tagihan) {
						System.out.println(" ");

						System.out.println("=== Pembayaran berhasil ===");
						System.out.println("=== Terimakasih ===");
					} else if (bayar > total_tagihan) {
						kembalian = bayar - total_tagihan;
						System.out.println(" ");
						System.out.println("=== Uang kembalian anda : Rp " + kembalian + " ===");
						System.out.println("=== Pembayaran berhasil ===");
						System.out.println("=== Terimakasih ===");
						System.out.println(" ");
						System.out.println("Pesan lagi : ");
						String konfir = in.next();
						if(konfir.equals("Tidak")){
							x = false;
						}
					}

					ayam = 0;
					ikan = 0;
					cumi = 0;
					break;
				case 3:
					System.exit(0);
					break;
			}

		}
	}
}
